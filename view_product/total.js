let inputqt = document.getElementsByClassName("inputqt");
// eventListener onChange
for (const input of inputqt) {
  input.addEventListener("change", (event) => {
    majTotal(inputqt);
  });
}

let total = document.getElementById("total");
let totalkg = document.getElementById("totalkg");
function majTotal(prods) {
  let somme = 0;
  let poids = 0;
  for (const input of prods) {
    let prix = input.getAttribute("data-prix");
    let kg = input.getAttribute("data-qt");
    let val = input.value;
    somme += prix * val;
    poids += kg * val * 2;
  }
  total.innerText = somme;
  totalkg.innerText = poids;
}
