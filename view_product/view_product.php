<?php include "../header/header.php"; ?>
<link rel="stylesheet" href="styles.css">
<?php
include "../debug/debug.php";
include "../BDD/data.php";
$testpro = test();

?>


<main>
    <?php
    $i = 0;
    foreach ($testpro as $f) {
    ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card mb-10">

                        <div class="card-body store-body">
                            <div class="product-payment-details">
                                <div class="containerDetail">
                                    <div class="desc1">
                                        <p class="parag">Prix:<?php echo $f['prix']; ?>$</p>

                                    </div>

                                    <div class="help">
                                        <div class="box1">

                                            <div class="soutext">
                                                <p class="total">Retrait en magazin :</p>
                                            </div>

                                            <div class="prix1">
                                                <label class="iii">ouvert 24/24h & 7/7j
                                                    <input type="checkbox" class="checkbox-round" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="box2">

                                            <div class="soutex2">
                                                <p class="promo">Livraison Papa-Noel:</p>
                                            </div>

                                            <div class="prix2">
                                                <label class="iii">lundi avant 02h00
                                                    <input type="checkbox" class="checkbox-round" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>


                                        </div>


                                    </div>
                                    <div class="button">
                                        <form class="form" action="../controller/ajoutPanier.php" method="post">
                                            <div class="htest">
                                                <label class="quantite_disp" for="tentacles" value="<?php echo $f['stock'] ?>">Quantité</label>
                                                <input class="inputqt" id='1' data-prix="<?php echo $f['prix']; ?>" data-qt="0.500" name="2" value="0" min="0" type="number" step='1' required>
                                                <p>Total : <span id="total">0</span>€</p>
                                            </div>



                                            <button type="submit" name="nom" value="<?php echo $_GET['product'] ?>" class="btn">Ajouter au panier </button>

                                            <input type="checkbox" name="si_livraison" class="checkbox-round" value="<?php echo $f['si_livraison']; ?>" />
                                        </form>
                                    </div>

                                    <!-- <p>Qt prise : <span id="totalkg">0</span></p> -->
                                    <p class="stock"><?php echo $f['stock']; ?> en stock en se moment </p>
                                </div>


                            </div>
                            <div class="product-info">
                                <H1 class="titre"><?php echo $f['nom']; ?></H1>
                                <div class="product-gallery">
       
                                <?php
            foreach (getimage($f['id']) as $image){
        ?>
                                    <div class="product-gallery-featured">
                                        <img src="../stock/uploads/<?php echo $image['url']?>" class="img-responsive" alt="">
                                    </div>
                                    <?php } ?>
                                </div>

                                <div class="product-description mb-5">

                                    <h2 class="mb-5">Description</h2>
                                    <p class="desc"><?php echo $f['description']; ?></p>



                                    <h2 class="mb-5">Entretien</h2>
                                    <dl class="row mb-5">
                                        <dt class="col-sm-3">Temps</i></dt>
                                        <dd class="col-sm-9"><?php echo $f['temps']; ?></dd>

                                        <dt class="col-sm-3">Temperature</dt>
                                        <dd class="col-sm-9"><?php echo $f['temperature']; ?></dd>

                                        <dt class="col-sm-3">Humidite</dt>
                                        <dd class="col-sm-9"><?php echo $f['humidite']; ?></dd>

                                        <!-- <dt class="col-sm-3">Fabric</dt>
                                    <dd class="col-sm-9">Cottom</dd> -->
                                    </dl>

                                </div>

                            <?php } ?>
                            <!-- carousel">
                                
                                /.carousel end -->



                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        </div>
</main>
<script src="total.js"></script>
<script type="text/javascript" src="https://your-site-or-cdn.com/fontawesome/v5.15.4/js/conflict-detection.js"></script>
<script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous">
</script>
<?php include "../footer/footer.php"; ?>