<?php session_start();
if (isset($_SESSION['panier'])) {
    include '../header/header.php';

?>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="styles.css">
        <title>Test</title>
    </head>

    <body>
        <h1>Votre panier</h1>

        <div class="info">
            <p>Voulez-vous vous identifier a votre Compte ou en créer un <a href="../sign-up-login/Sing-up-login.php">suivez ce lien </a>!!!</p>
        </div>

        <div class="cart">
            <div class="shopping-cart">

                <div class="column-labels">
                    <label class="product-image">Image</label>
                    <label class="product-details">Product</label>
                    <label class="product-price">Price</label>
                    <label class="product-quantity">Quantity</label>
                    <label class="product-removal">Remove</label>
                </div>

                <?php
                include "../BDD/data.php";
                // include "../debug/debug.php";
                $panier = $_SESSION['panier'];
                $total = 0;
                $kg = 0;
                foreach ($panier as $produit) {
                    $id = $produit['id'];
                    $quantite = $produit['quantite'];
                    $select = getProduitById($id)[0];
                    $prixUnProduit = 0;
                    $prixUnProduit = $quantite * $select['prix'];
                    $total += $prixUnProduit;
                    $kg += $quantite;

                    // print_r ($_SESSION['panier']);

                ?>
                <!-- <form action="../controller/cart_edit.php"> -->
                    <div class="product">
                        <?php
                        foreach (getimage($select['id']) as $image) {
                        ?>
                            <div class="product-image">
                                <img src="../stock/uploads/<?php echo $image['url'] ?>" class="product-image">
                            </div>
                        <?php } ?>
                        <div class="product-details">
                            <div class="product-title" name="product-title"><?php echo $select['nom'] ?></div>
                            <p class="product-description">T<?php echo $select['descriptionMini'] ?></p>
                        </div>
                        <div class="product-price"><?php echo $select['prix'] ?></div>
                        <div class="product-quantity">
                                <input type="number" value="<?php echo $quantite ?>" min="1" onchange="change(this)" name="quantity">
                                <input type="hidden" name="id" value=""<?php echo $id?>>
                        </div>
                        <div class="product-removal">
                            <a href="../controller/deletepanier.php?action=delete&id=<?php echo $select["id"]; ?>"><span class="text-danger">Remove</span></a></td>
                        </div>

                    </div>

                <?php  }


                ?>


            </div>
            <div id="centrecontainer">
                <p id="parag"></p>
                <div class="flex-columns" id="recap">
                    <p><?php echo $select['nom'] ?> </p><p>x <?php echo $quantite ?></p>
                </div>
                <div class="flex-row border-top">
                    <p class="total">Total:</p>
                    <p id="cart-subtotal" class="totals-value"><?php echo $total ?></p>
                </div>
                <?php if (isset($produit['solde'])) { ?>
                    <div class="box2">

                        <div class="soutex2">
                            <p class="promo">promo:</p>
                        </div>

                        <div class="prix2">
                            <p class="prix3">????</p>
                        </div>

                    </div>
                <?php } ?>
                <div class="button">
                <form action="../ConnexionPayment/Sing-up-login.php">
                        <button href="" type="submit" class="btn">Valider mon panier </button>
                    </form>
                </div>


            </div>

        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="app.js"></script>
        <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
    </body>
<?php } else {
    // print_r("<p style='color : red'>Vous n'avez rien ajouter dans votre panier</p>");
    echo'
    <link rel="stylesheet" href="styles.css">
    <body id = "bodyOfPaniervide">
    <div class="cart-empty">
    <div class="empty-cart-header">
    </div>
        <div class="empty-cart">
            <a href="../boutique/boutique.php"><span class="empty-cart-icon"><img src = "https://www.agnassan.com/images/empty.png"></span></a>
            <h2 class = "h2RED">VOTRE PANIER EST ACTUELLEMENT VIDE!</h2>
          <form method="get" action="../boutique/boutique.php">
    <button class ="buttonContinuer" type="submit">Continuer vos achats</button>
</form>
        </div>
 </div>*
 </body>
 ';

} ?>

    </html>