<link rel="stylesheet" href="../footer/footer.css">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<div id="divTopFooterKK"></div>
<footer>
    <div class="bigDivFooter">
        <div class="listFooter" id="containerLeftFooter">
            <ul class="classUlFooter">
                <li class="liFooter"><a href="../home/index.php">Accueil</a></li>
                <li class="liFooter"><a href="../boutique/boutique.php">Boutique</a></li>
                <li class="liFooter"><a href="../contact/contact.php">Contact</a></li>
                <li class="liFooter"><a href="../sign-up-login/Sing-up-login.php">Connexion</a></li>
                <li class="liFooter"><a href="../actualites/actualites.php">Actualites</a></li>
            </ul>
        </div>
    </div>
    <div class="bigDivFooter">
        <div id="containerMiddleFooter1">
            <div class="divMiddleLink">
                <a href="https://www.facebook.com/Bison.affute11"><img class="logoFooterLink" width="50"
                        src="https://i.pinimg.com/originals/d3/d1/75/d3d175e560ae133f1ed5cd4ec173751a.png" alt="logo_pinterest
                            " /></a>
            </div>
            <div class="divMiddleLink">
                <a href="https://www.youtube.com/channel/UCCskTYJncIHcUikjWXHhaGw"><img class="logoFooterLink"
                        width="50"
                        src="https://upload.wikimedia.org/wikipedia/fr/thumb/c/c8/Twitter_Bird.svg/1200px-Twitter_Bird.svg.png"
                        alt="twitter
                            " /></a>
            </div>
            <div class="divMiddleLink">
                <a href="https://www.instagram.com/?hl=en"><img class="logoFooterLink" width="50"
                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a5/Instagram_icon.png/2048px-Instagram_icon.png"
                        alt="logo_instagram
                            " /></a>
            </div>
        </div>
        <div id="containerMiddleFooter2">
            <div class="divMiddleCopyRight">
                <p>Copyright 2022 Plantera, Narbonne</p>
            </div>
        </div>
    </div>
    <div class="bigDivFooter" id="divFooterRightContact">
        <div class="containerRightFooter">
            <p id="numFooter"><a href="tel:06.09.57.58.48">+55 61 0123 456</a></p>
        </div>
        <div class="containerRightFooter">
            <p id="adresseFooter"><a href="https://goo.gl/maps/ACmyF2HPXL3sBj7B9">Plantera</a></p>

        </div>
    </div>
</footer>

<script src="../home/home.js"></script>
</body>

</html>