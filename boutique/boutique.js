const gallery2 = document.querySelector("#paginated_gallery");
const gallery_scroller2 = gallery2.querySelector(".gallery_scroller");
const gallery_item_size2 = gallery_scroller2.querySelector("div").clientWidth;

gallery2.querySelector(".btn.next").addEventListener("click", scrollToNextPage);
gallery2.querySelector(".btn.prev").addEventListener("click", scrollToPrevPage);

function scrollToNextPage() {
  gallery_scroller2.scrollBy(gallery_item_size2, 0);
}
function scrollToPrevPage() {
  gallery_scroller2.scrollBy(-gallery_item_size2, 0);
}

function updateAlignment(event) {
  const alignment = event.target.value;
  for (item of gallery2.querySelectorAll(".gallery_scroller > div"))
    item.style.scrollSnapAlign = alignment;
}