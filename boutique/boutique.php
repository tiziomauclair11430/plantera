<?php include "../header/header.php"; ?>
<h2 class="king">Boutique</h2>


<link rel="stylesheet" href="../boutique/boutique.css">
<?php
    session_start();
    // $_SESSION = array();
   
    // session_destroy();

    
     include "../debug/debug.php";
    include "../BDD/data.php";
        $list_pro = getProduitAndImage();

?>
<div class="avant-page">
    <?php foreach($list_pro as $select){ ?>
        <div class="product-card">
            <a class="card-description" href="../view_product/view_product.php?product=<?php echo $select['id'] ?>">
            <img src="../stock/uploads/<?php echo $select['url']?>"  class="img-product">
            <div class = "description">
			    <h3 class = "titre"><?php echo $select['nom'] ?></h3>
			    <p><?php echo $select['descriptionMini'] ?></p>
		    </a>
            <p class = "prix"><?php echo $select['prix'] ?>$</p>
            </div>
        </div>
    <?php } ?>
</div>



<script src="../boutique/boutique.js"></script>

<?php include "../footer/footer.php"; ?>