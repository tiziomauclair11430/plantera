<!doctype html>
<html lang="en">
<?php session_start() ?>
<head>
    <title>Header</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../header/header.css">
    <script src="https://kit.fontawesome.com/332a215f17.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/fontawesome.min.css"
        integrity="sha512-xX2rYBFJSj86W54Fyv1de80DWBq7zYLn2z0I9bIhQG+rxIF6XVJUpdGnsNHWRa6AvP89vtFupEPDP8eZAtu9qA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css"
        integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <section>
        <div class="cover">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 d-flex align-items-center">
                        <p class="mb-0 phone pl-md-2">
                            <a href="#" class="mr-2">
                                <i class="fas fa-phone mr-1"></i>
                                +55 61 0123 456
                            </a>
                            <a href="#"><i class="fas fa-paper-plane mr-1"></i>
                                plantera505@gmail.com
                            </a>
                        </p>
                    </div>
                    <div class="reg">
                    
                        <?php 
                        if(!isset($_SESSION['compte'])){
                            print_r('<a href="../sign-up-login/Sing-up-login.php" class="mr-2 mb-0">Inscription</a>');
                        }else if(isset($_SESSION['compte']['client']) && !isset($_SESSION['compte']['admin'])){
                            print_r('<a href="../client/espace_client.php#dashboard" class="mr-2 mb-0">Re-bonjour '.$_SESSION['compte'].'</a>');
                        }else if(isset($_SESSION['compte']['admin']) && !isset($_SESSION['compte']['client'])){
                            print_r('<a href="../admin/espace_admin.php#dashboard" class="mr-2 mb-0">Re-bonjour '.$_SESSION['compte']['pseudo'].'</a>');
                        }else{
                            print_r('<a href="../sessiondelete/sessiondelete.php" style="color:red">ERROR : Veuillez cliquer ici </a>');
                        }
                        
                        ?>
                    </div>
                </div>
            </div>

        </div>
        </div>
        <?php 

(isset($_SESSION['panier']));

function my_shopping_cart_total_product_count() {
    $product_count = 0;

    if ( isset( $_SESSION['panier'] ) ) {
        $product_count = count($_SESSION['panier']);
       
    //    array_sum( array_column( $_SESSION['panier'], 'quantite' ) );
    } 

    return $product_count;
}

?>
        <!--Nav-->
        <nav class="navbar navbar-expand-lg main-navbar bg-color main-navbar-color" id="main-navbar">
            <div class="container mobile">
                <img class="planteraImg" src="../stock/ress/Logo_Plantera.png">
                <div class="order-lg-last btn-group">
                    <a href="../panier/panier.php" id="">
                        <i class="fa-solid fa-cart-shopping fa-2x"></a></i><?php if(my_shopping_cart_total_product_count() !== 0){echo "<span class='count'>".my_shopping_cart_total_product_count()."</span>"; }else{echo "";}?>
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNav"
                    aria-controls="nav" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fas fa-bars"></i>
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse" id="myNav">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a href="../home/index.php" class="nav-link">Accueil</a>
                        </li>
                        <li class="nav-item">
                            <a href="../boutique/boutique.php" class="nav-link">Boutique</a>
                        </li>
                        <li class="nav-item">
                            <a href="../contact/contact.php" class="nav-link">Contact</a>
                        </li>
                        <li class="nav-item">
                            <a href="../actualites/actualites.php" class="nav-link">Actualites</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!--End of Nav-->
    </section>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>