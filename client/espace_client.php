<?php
session_start();
$compte = $_SESSION['compte'];
include '../BDD/data.php';
include '../protected/protected.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Espace Client</title>
    <link rel="stylesheet" href="espace_client.css">
</head>

<body>
    <?php foreach (selectClientNom($compte['pseudo']) as $select) { ?>
    <div class="admin-panel clearfix">
        <div class="slidebar">
            <div class="logo">
            <a href="espace_client.php#Modifier-mon-profil">
                <img id="avatar" src="<?php echo $select['photo_profil'] ?>">
                </a>
            </div>

            <ul>

                <li><a href="#dashboard" id="targeted">Dashboard</a></li>
                <li><a href="#mon-profil">Mon profil</a></li>
                <li><a href="#Modifier-mon-profil">Modifier mon profil</a></li>
                <li><a href="#supprimer-mon-profil">supprimer mon profil</a></li>
                <li><a href="#voir-commande">voir-commande</a></li>
                <li><a href="../home/index.php">Retour boutique</a></li>
                <li><a href="../sessiondelete/sessiondelete.php">Deconnexion</a></li>

            </ul>
        </div>

        <div class="main">
            <div class="mainContent clearfix">
                <div id="dashboard">
                    <h2 class="header"><span class="icon"></span>Dashboard</h2>
                    <div class="welcome-card">

                        <h1>Bienvenue <span><?php echo $select['pseudo']; ?></span></h1>
                        <p>
                            ✌️Hey, je vous souhaite la bienvenue sur votre espace client.
                        </p>
                    </div>
                </div>
                <div id="mon-profil">
                    <h2 class="header">Mon profil</h2>

                    <div class="container">
                        <div id="client">

                            <p class="nomClient">Nom <strong><?php echo $select['pseudo']; ?></strong></p>
                            <p class="numero">Numero de telephone : <strong><?php echo $select['numero_de_tel']; ?>
                                </strong></p>
                            <p class="adresse">Adresse mail : <strong><?php echo $select['addresse']; ?> </strong></>

                            <form action="../client/espace_client.php#Modifier-mon-profil" method="post">
                                <button type="submit" name="modif" value="<?php echo $select['id'] ?>"><i
                                        class="fas fa-pen"></i>Modifier</button></button>
                            </form>

                        </div><!-- cleint -->
                    </div><!-- /.container -->
                    <?php } ?>
                </div>
                <div id="Modifier-mon-profil">
                    <h2 class="header">Modifier mon profil</h2>
                    <?php foreach (selectClientNom($compte['pseudo']) as $select) { ?>
                    <form class="" action="../controller/updateClient.php" method="post" enctype="multipart/form-data">

                        <p>img</p>
                        <div class="profile-pic">
                            <label class="-label" for="file">
                                <span class="glyphicon glyphicon-camera"></span>
                                <span>Change Image</span>
                            </label>
                            <input type="file" name="file" id="file" name="photo" accept="image/*"
                                onchange="loadFile(event)" />

                            <img id="output" width="200" src="<?php echo $select['photo_profil'] ?>" />

                        </div>


                        <p>nom :</p>
                        <input type="text" id="nom" name="nom" class="input-product"
                            value="<?php echo $select['pseudo']; ?>">

                        <p>Numero de telephone :</p>
                        <input type="text" id="Numero-de-telephone" name="Numero-de-telephone" class="input-product"
                            value="<?php echo $select['numero_de_tel']; ?>">

                        <p>Adresse mail :</p>
                        <input type="text" id="mail" name="mail" class="input-product"
                            value="<?php echo $select['addresse']; ?> ">

                        <button class="" type="submit" name="id" value="<?php echo $select['id']; ?>">Modifier
                        </button>
                    </form>
                    <?php } ?>


                </div>
                <div id="supprimer-mon-profil">
                    <h2 class="header">Supprimer mon profil</h2>
                    <p>Pour supprimer votre compte, remplisser le champ avec vote mots de passe.</p>
                    <form class="form" method="post" action="../controller/deleteAccountClient.php">
                        <input type="hidden" placeholder="Login" value="<?php echo $select['pseudo'] ?>" name="pseudo"
                            id="pseudo">
                        </p>mdp:</p>
                        <input type="password" placeholder="Password" required="" name="mdp" id="mdp">
                        <button type="submit" name="supp" id="valid" value="<?php echo $select['id'] ?>"><i
                                class="fas fa-trash"></i></button>
                </div>
                </form>
                <div id="voir-commande">
                    <h2 class="header">voir-commande</h2>
                </div>

            </div>
        </div>
    </div>

    <script src="app.js"></script>
    <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
</body>

</html>