<?php 
session_start();
(isset($_SESSION['panier']));
include "../header/header.php";
include('include/header.php');
include('protected/protected.php');

?>
<title>Stripe Payment </title>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript"
    src="https://cdnjs.cloudflare.com/ajax/libs/jquery-creditcardvalidator/1.0.0/jquery.creditCardValidator.js">
</script>
<script type="text/javascript" src="script/payment.js"></script>
<?php include('include/container.php');?>
<div class="container">
    <div class="row">
        <h2>Payment </h2>

        <?php 
		if(isset($_SESSION["message"]) && $_SESSION["message"] && $_SESSION["message"] == 'failed') {
		?>
        <div class="alert alert-danger">
            <?php 
			  echo "Error : Payment failed!"; 
			  $_SESSION["message"] = '';
			  ?>
        </div>
        <?php 
		} elseif(isset($_SESSION["message"]) && $_SESSION["message"]) {
		?>
        <div class="alert alert-success">
            <?php 
			  echo $_SESSION["message"]; 
			  $_SESSION["message"] = '';
			  ?>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">Processus de commande</div>
            <div class="panel-body">
                <form action="process.php" method="POST" id="paymentForm">
                    <div class="row">
                        <div class="col-md-8" style="border-right:1px solid #ddd;">
                            <h4 align="center">Détails du client</h4>
                            <div class="form-group">
                                <label><b>Card Holder Name <span class="text-danger">*</span></b></label>
                                <input type="text" name="customerName" id="customerName" class="form-control" value="">
                                <span id="errorCustomerName" class="text-danger"></span>
                            </div>
                            <div class="form-group">
                                <label><b>Email Address <span class="text-danger">*</span></b></label>
                                <input type="text" name="emailAddress" placeholder="example@gmail.com" id="emailAddress" class="form-control" value="">
                                <span id="errorEmailAddress" class="text-danger"></span>
                            </div>
                            <div class="form-group">
                                <label><b>Address <span class="text-danger">*</span></b></label>
                                <textarea name="customerAddress" id="customerAddress" class="form-control"></textarea>
                                <span id="errorCustomerAddress" class="text-danger"></span>
                            </div>
                            <div class="form-group">
                                <label><b>Phone <span class="text-danger">*</span></b></label>
                                <textarea name="customerPhone"  placeholder="+33757690094" id="customerPhone" class="form-control"></textarea>
                                <span id="errorCustomerPhone" class="text-danger"></span>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><b>City <span class="text-danger">*</span></b></label>
                                        <input type="text" name="customerCity" id="customerCity" class="form-control"
                                            value="">
                                        <span id="errorCustomerCity" class="text-danger"></span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><b>Zip <span class="text-danger">*</span></b></label>
                                        <input type="text" name="customerZipcode" id="customerZipcode"
                                            class="form-control" value="">
                                        <span id="errorCustomerZipcode" class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><b>State </b></label>
                                        <input type="text" name="customerState" id="customerState" class="form-control"
                                            value="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label><b>Country <span class="text-danger">*</span></b></label>
                                        <input type="text" name="customerCountry" id="customerCountry"
                                            class="form-control">
                                        <span id="errorCustomerCountry" class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <h4 align="center">Payment Details</h4>
                            <div class="form-group">
                                <label>Card Number <span class="text-danger">*</span></label>
                                <input type="text" name="cardNumber" id="cardNumber" class="form-control"
                                    placeholder="1234 5678 9012 3456" maxlength="20" onkeypress="">
                                <span id="errorCardNumber" class="text-danger"></span>
                                <cite>Pour test : 5555555555554444</cite>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Expiry Month</label>
                                        <input type="text" name="cardExpMonth" id="cardExpMonth" class="form-control"
                                            placeholder="MM" maxlength="2" onkeypress="return validateNumber(event);">
                                        <span id="errorCardExpMonth" class="text-danger"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Expiry Year</label>
                                        <input type="text" name="cardExpYear" id="cardExpYear" class="form-control"
                                            placeholder="YYYY" maxlength="4" onkeypress="return validateNumber(event);">
                                        <span id="errorCardExpYear" class="text-danger"></span>
                                    </div>
                                    <div class="col-md-4">
                                        <label>CVC</label>
                                        <input type="text" name="cardCVC" id="cardCVC" class="form-control"
                                            placeholder="123" maxlength="4" onkeypress="return validateNumber(event);">
                                        <span id="errorCardCvc" class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <?php
    include "../BDD/data.php";
    // include "../debug/debug.php";
    $panier = $_SESSION['panier'];
    $total = 0;
    $kg = 0;
    foreach ($panier as $produit) {
        $id = $produit['id'];
        $quantite = $produit['quantite'];
        $select = getProduitById($id)[0];
        $prixUnProduit = 0;
        $prixUnProduit = $quantite * $select['prix'];
        $total += $prixUnProduit; 
        $kg += $quantite;

       
         

       
    ?>

                            <div align="center">
                                <input type="hidden" name="price" value="<?php echo $select['prix'] ?>">
                                <input type="hidden" name="quantite" value="<?php echo $quantite ?>">
                                <input type="hidden" name="total_amount" value="<?php echo $prixUnProduit ?>">
                                <input type="hidden" name="currency_code" value="USD">
                                <input type="hidden" name="item_details" value="<?php echo $select['nom'] ?>">
                                <input type="hidden" name="item_number" value="TS1234567">
                                <input type="hidden" name="order_number" value="SKA987654321">

                            </div>

                            <br>
                        </div>
                        <div class="col-md-4">
                            <!-- <h4 align="center">Order Details</h4> -->
                            <div class="table-responsive" id="order_table">
                                <table class="table table-bordered table-striped">
                                    <tbody>
                                        <!-- <tr>
                                            <th colspan="1">
                                                <h4 align="center">Order Details</h4>
                                            </th>
                                        <tr> -->
                                        <th>Product Name</th>
                                        <th>Quantity</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                        </tr>
                                        <tr>
                                            <td><strong><?php echo $select['nom'] ?></strong></td>
                                            <td><?php echo $quantite ?></td>
                                            <td align="right"><?php echo $select['prix'] ?></td>
                                            <td align="right"><?php echo $total ?></td>
                                            <?php  } 
            
            
            ?>
                                        </tr>

                                        <tr>
                                            <td colspan="3" align="right">Total Global</td>
                                            <td align="right"><strong>$ <?php echo $total ?></strong></td>
                                        </tr>

                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>
                    <input type="button" name="payNow" id="payNow" class="btn btn-success btn-sm"
                        onclick="stripePay(event)" value="Pay Now" />
                </form>
            </div>
        </div>
    </div>
</div>
<?php include('include/footer.php');
// include "../footer/footer.php"?>
