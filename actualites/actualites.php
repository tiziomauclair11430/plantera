<?php
    // $_SESSION = array();
   
    // session_destroy();

    
    //  include "../debug/debug.php";
    

?>


<?php include "../header/header.php"; ?>
<link rel="stylesheet" href="actualites.css">
<h2 class="king">Actualites</h2>


<?php 
       include "../BDD/data.php";
       $list_actualites = getActualites();
      
      ?>

<div class="projcard-container">

    <?php
            foreach($list_actualites as $select){
        ?>

    <div class="projcard projcard-customcolor" style="--projcard-color: #F5AF41;">
        <div class="projcard-innerbox">
            <img class="projcard-img" src="../stock/uploads/<?php echo $select['photo']?>" />
            <div class="projcard-textbox">
                <div class="projcard-title"><?php echo $select['title'];?></div>
                <div class="projcard-subtitle"><?php echo $select['sous_title'];?></div>
                <div class="projcard-bar"></div>
                <div class="projcard-description"><?php echo $select['card_description'];?></div>
                <div class="projcard-tagbox">
                    <span class="projcard-tag"><?php echo $select['mots_cles1'];?></span>
                    <span class="projcard-tag"><?php echo $select['mots_cles2'];?></span>
                    <span class="projcard-tag"><?php echo $select['mots_cles3'];?></span>

                </div>
                <div class="boxDA">
                    <a href="../view_actualite/view_actualite.php?actualites=<?php echo $select['id'] ?>">Voir plus</a>
                    <span class="date"><?php echo $select['Ladate'] ?><span>
                </div>
            </div>

        </div>

    </div>

    <?php } ?>
</div>


<?php include "../footer/footer.php"; ?>