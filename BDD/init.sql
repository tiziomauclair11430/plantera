DROP DATABASE IF EXISTS Plantera;
CREATE DATABASE Plantera;
USE Plantera;
GRANT ALL PRIVILEGES ON ti.* TO 'ti'@'localhost';

CREATE TABLE product (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(120),
    description LONGTEXT,
    descriptionMini varchar(125),
    temperature VARCHAR(260),   
    humidite VARCHAR(260),
    temps VARCHAR(260),
    stock INT,
    prix float(10,2),
    si_solde BOOLEAN,
    prix_solde VARCHAR(30),
    si_livraison BOOLEAN,
    dateproduct DATE
);

CREATE TABLE stock_image (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    url VARCHAR(120)
);

CREATE TABLE carousel (
    id_image INT UNSIGNED,
    id_carrousel INT UNSIGNED,
    UNIQUE(id_image,id_carrousel),
    FOREIGN KEY (id_image) REFERENCES stock_image(id),
    FOREIGN KEY (id_carrousel) REFERENCES product(id)
);

CREATE TABLE utilisateur (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    pseudo VARCHAR(120),
    addresse LONGTEXT,
    numero_de_tel VARCHAR(120),
    mdp VARCHAR(270),
    photo_profil VARCHAR(255)
);




CREATE TABLE Admin0 (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    pseudo VARCHAR(120),
    mdp VARCHAR(270)
);

CREATE TABLE contact (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nom VARCHAR(25),
    email VARCHAR(35),
    comment VARCHAR(500),
    datemail DATE
);

INSERT INTO Admin0 (pseudo, mdp) VALUES ("admin", "test");


CREATE TABLE actualites (
     id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
     title VARCHAR(120),
     sous_title  VARCHAR(60),
     card_description VARCHAR(400),
     mots_cles1 VARCHAR(25),
     mots_cles2 VARCHAR(25),
     mots_cles3 VARCHAR(25),
     paragraph1 LONGTEXT,
     paragraph2 LONGTEXT,
     paragraph3 LONGTEXT,
     photo VARCHAR(255),
     author VARCHAR(120),
     Ladate DATE
);


CREATE Table transactions (
 id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 cust_name varchar(100),
 cust_email varchar(255) ,
 customerAddress varchar(255) , 
 customerPhone varchar(255) ,
 customerCity varchar(255) ,
 customerZipcode varchar(255) ,
 customerState varchar(255) ,
 customerCountry varchar(255),
 card_number bigint(20),
 card_cvc int(5),
 card_exp_month varchar(2),
 card_exp_year varchar(5),
 item_name varchar(255),
 item_number varchar(50),
 item_price float(10,2),
 quantite varchar(255),
 item_price_currency varchar(10) DEFAULT 'usd',
 paid_amount varchar(10),
 paid_amount_currency varchar(10),
 txn_id varchar(100),
 payment_status varchar(50),
 created datetime,
 modified datetime 
);

CREATE TABLE commande_product (
    id_produit INT UNSIGNED,
    id_transaction INT UNSIGNED,
    UNIQUE(id_produit,id_transaction),
    FOREIGN KEY (id_produit) REFERENCES product(id),
    FOREIGN KEY (id_transaction) REFERENCES transactions(id),
    mode_livraison BOOLEAN
);