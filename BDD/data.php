<?php
$host = 'localhost';
$db = 'Plantera';
$user = 'ti';
$pass = 'mauclair';

try{
$pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}
catch(PDOException $e){
    echo $e->getMessage()
;}

// *===* CREATE *==* 

function addCoordonneesClient($pseudo,$addresse,$numero_de_tel,$mdp,$PP) {
    global $pdo;
    $req = $pdo->prepare('INSERT INTO utilisateur (pseudo,addresse,numero_de_tel,mdp,photo_profil) VALUES(?,?,?,?,?)');
    $req->execute([$pseudo,$addresse,$numero_de_tel,$mdp,$PP]);
};


function selectAdmin($nom, $mot_de_passe){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM Admin0 WHERE pseudo = ? AND mdp = ?');
    $req->execute([$nom, $mot_de_passe]);
    return $req->fetchAll();
};


function selectClient($nom, $mot_de_passe){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM utilisateur WHERE pseudo = ? AND mdp = ?');
    $req->execute([$nom, $mot_de_passe]);
    return $req ->fetchAll();
};

function selectClientNom($nom){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM utilisateur WHERE pseudo = ?');
    $req->execute([$nom]);
    return $req->fetchAll();
};

function CheckExistClientName($nom){
    global $pdo;
    $stmt = $pdo->prepare("SELECT * FROM utilisateur WHERE pseudo=?");
    $stmt->execute([$nom]);
    $user = $stmt->fetch();
    if ($user) {
        return true;
    } else {
        return false;
    }
};

// function selectAdminNom($nom){
//     global $pdo;
//     $req = $pdo->prepare('SELECT * FROM Admin0 WHERE pseudo = ? ');
//     $req->execute([$nom]);
//     return $req->fetchAll();
// };

// function selectClientAll(){
//     global $pdo;
//     $req = $pdo->query('SELECT * FROM utilisateur');
//     return $req->fetchAll();
// };


function addProduit($nom, $description, $Minidescription, $temperature,$humidite,$temps, $stock,$prix,$si_solde,$Prixsolde,$livraison){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO product(nom, description, descriptionMini, temperature, humidite,temps,stock,prix,si_solde,prix_solde,si_livraison,dateproduct) VALUES(?,?,?,?,?,?,?,?,?,?,?,NOW())');
    $req->execute([$nom, $description, $Minidescription, $temperature,$humidite,$temps, $stock,$prix,$si_solde,$Prixsolde,$livraison]);   
    return $pdo->lastInsertId();
};

function addImage($img){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO stock_image(url) VALUES(?)');
    $req->execute([$img]); 
    return $pdo->lastInsertId();  
};

function liaison_carousel($id_product,$id_images){
    global $pdo;
    $req = $pdo->prepare('INSERT INTO carousel(id_image,id_carrousel) VALUES(?,?)');
    $req->execute([$id_images,$id_product]); 
}


function getProduit(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM product');
    return $req->fetchAll();
};

function getProduitAndImage(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM carousel INNER JOIN product ON carousel.id_carrousel = product.id INNER JOIN stock_image ON carousel.id_image = stock_image.id');
    return $req->fetchAll();
}

function getImage($id_product){
    global $pdo;
    $req = $pdo->prepare('SELECT * FROM stock_image INNER JOIN carousel ON id = id_image WHERE id_carrousel = ?');
    $req->execute([$id_product]);
    return $req->fetchAll();
};

function test(){
    global $pdo;
    $req = $pdo->query('SELECT * FROM product WHERE id = "'.$_GET['product'].'"');
    return $req->fetchAll();
};


function getProduitById($id){
    global $pdo; 
    $req = $pdo->prepare('SELECT * FROM carousel INNER JOIN product ON carousel.id_carrousel = product.id INNER JOIN stock_image ON carousel.id_image = stock_image.id WHERE product.id = ?');
    $req->execute([$id]);
    return $req->fetchAll();
};

function getAllIdProduct(){
    global $pdo; 
    $req = $pdo->query('SELECT id,nom FROM product');
    return $req->fetchAll();
}


function deleteProduit($effacer){
    global $pdo;
    $req = $pdo->prepare('DELETE FROM carousel WHERE id_carrousel = ?');
    $req->execute([$effacer]);
    $req = $pdo->prepare('DELETE FROM product WHERE id = ?');
    $req->execute([$effacer]);

};

function deleteClient($effacer){
    global $pdo;
    try{
        $req = $pdo->prepare('DELETE FROM utilisateur WHERE id = ?');
        $req->execute([$effacer]); 
    }catch(Exception $e){
            // en cas d'erreur :
            echo " Erreur ! ".$e->getMessage();
            echo $req;
    }
};

function updateProduit($nom, $description, $Minidescription, $temperature,$humidite,$temps, $stock,$prix,$si_solde,$Prixsolde,$livraison , $id){
    global $pdo;
    try{
    $req = $pdo->prepare('UPDATE product SET nom = ?,description = ?, descriptionMini = ?, temperature = ?,humidite = ? ,temps = ?,stock = ? ,prix = ?,si_solde = ?,prix_solde = ?,si_livraison = ? WHERE id = ?');
    $req->execute([$nom, $description, $Minidescription, $temperature,$humidite,$temps, $stock,$prix,$si_solde,$Prixsolde,$livraison , $id]);
    }catch(Exception $e){
        // en cas d'erreur :
         echo " Erreur ! ".$e->getMessage();
         echo $req;
      }
    
};

function updateClient($nom, $NumeroDeTelephone, $mail  , $photo,$id ){
    global $pdo;
    $req = $pdo->prepare('UPDATE utilisateur
    SET pseudo = ?,numero_de_tel = ?, addresse = ? , photo_profil = ? WHERE id = ?');
    $req->execute([$nom, $NumeroDeTelephone, $mail  ,$photo,$id]);
   
}


// function updateClient($nom, $NumeroDeTelephone, $mail , $id){
//     global $pdo;
//     $req = $pdo->prepare('UPDATE utilisateur
//     SET pseudo = ?,numero_de_tel = ?, addresse = ?,photo_profil = ? WHERE id = ?');
//     $req->execute([$nom, $NumeroDeTelephone, $mail , $id]);
   
// }

// *===* Mail *==*

function InsertContact($name,$email,$comment){
    global $pdo;
    $req = $pdo->prepare("INSERT into contact (nom,email,comment,datemail) values (?,?,?,NOW());");
    $req->execute([$name,$email,$comment]);
};

function getAllMail(){ 
global $pdo;
$req = $pdo->query("SELECT * FROM contact");
return $req->fetchAll();
}

function deleteMail($effacer){
    global $pdo; 
    $delete = $pdo->prepare("DELETE FROM contact WHERE id = ?");
    $delete->execute([$effacer]);
     }



     // *===* Actualites *==* 

     function addActualites($title, $sous_title, $card_description, $mots_cles1,$mots_cles2,$mots_cles3,$paragraph1,$paragraph2,$paragraph3,$photo,$author){
        global $pdo;
        $req = $pdo->prepare('INSERT INTO actualites(title, sous_title, card_description, mots_cles1, mots_cles2,mots_cles3,paragraph1,paragraph2,paragraph3,photo,author,Ladate) VALUES(?,?,?,?,?,?,?,?,?,?,?,NOW())');
        $req->execute([$title, $sous_title, $card_description, $mots_cles1,$mots_cles2,$mots_cles3,$paragraph1,$paragraph2,$paragraph3,$photo,$author,]);   
        return $pdo->lastInsertId();
    };


    function getActualites(){
        global $pdo;
        $req = $pdo->query('SELECT * FROM actualites');
        return $req->fetchAll();
    };


    function actualite_view(){
        global $pdo;
        $req = $pdo->query('SELECT * FROM actualites WHERE id = "'.$_GET['actualites'].'"');
        return $req->fetchAll();
    };


    function deleteActualites($effacer){
        global $pdo;
        $req = $pdo->prepare('DELETE FROM actualites WHERE id = ?');
        $req->execute([$effacer]);
    };


    function getctualitesById($id){
        global $pdo; 
        $req = $pdo->prepare('SELECT * FROM actualites WHERE id = ?');
        $req->execute([$id]);
        return $req->fetchAll();
    };


    function updateActualites($title, $sous_title, $card_description, $mots_cles1,$mots_cles2,$mots_cles3,$paragraph1,$paragraph2,$paragraph3,$photo,$author, $id){
        global $pdo;
        $req = $pdo->prepare('UPDATE actualites
        SET title = ?,sous_title = ?, card_description = ?, mots_cles1 = ?,mots_cles2 = ? ,mots_cles3 = ?,paragraph1 = ? ,paragraph2 = ?,paragraph3 = ?,photo=?,author=?
        WHERE  id = ?');
        $req->execute([$title, $sous_title, $card_description, $mots_cles1,$mots_cles2,$mots_cles3,$paragraph1,$paragraph2,$paragraph3,$photo,$author, $id]);
        
    };