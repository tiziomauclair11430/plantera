<?php
session_start();

include "../view/debug.php";
include '../BDD/data.php';

if(
    isset($_POST['pseudo']) !==""&&
    isset($_POST['mdp']) !=="" 
    )
  $nom = $_POST['pseudo']; 
  $pwd = $_POST["mdp"];
  if(password_verify($pwd,selectClientNom($nom)[0]["mdp"]) === true){
    echo 'client';
    session_destroy();
    unlink(selectClientNom($nom)[0]["photo_profil"]);
    deleteClient(selectClientNom($nom)[0]["id"]); 
    header('Location: ../home/index.php');
  }else{
    echo '<p style="color : red">Mot de passe invalide</p>';
  }