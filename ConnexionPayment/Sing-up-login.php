<!DOCTYPE html>
<html>
<?php 
session_start();
if(isset($_SESSION['compte']['client']) && !isset($_SESSION['compte']['admin'])){
    header("location:../stripe-payment-gateway-integration-php/pay.php");
}
?>

<head>
    <title>Login Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <div class="wrapper">
        <div class="headline">
            
               
                    <h1>Plantera-Paiement</h1>
        </div>
        <!-- Inscription-->
        <form class="form" method="post" action="../controller/Connexion_inscription_Payment.php">
            <div class="signup">
                <div class="form-group">
                    <input type="text" placeholder="Login" required="" name="pseudo_ins" id="pseudo_ins">
                </div>
                <div class="form-group">
                    <input type="email" placeholder="Email" required="" name="mail_ins" id="mail_ins">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" required="" name="mdp_ins" id="mdp_ins">
                </div>
                <div class="form-group">
                    <input type="tel" placeholder="Telephone" required="" name="num_ins" id="num_ins">
                </div>
                <button type="submit" class="btn">SIGN UP</button>
                <div class="account-exist">
                J'ai déjà un compte Plantera <a href="#" id="login">Login</a>
                </div>
            </div>
        </form>


        <!-- connexion-->
        <form class="form" method="post" action="../controller/Connexion_control_Payment.php">
            <div class=" signin">
                <div class="form-group">
                    <input type="text" placeholder="Login" required="" name="pseudo_con" id="pseudo_con">
                </div>
                <div class="form-group">
                    <input type="password" placeholder="Password" required="" name="mdp_con" id="mdp_con">
                </div>
                <div class="forget-password">
                    <div class="check-box">
                        <input type="checkbox" id="checkbox">
                        <label for="checkbox">Remember me</label>
                    </div>
                    <a href="#">Forget password?</a>
                </div>
                <button type="submit" class="btn">LOGIN</button>
                <div class="account-exist">
                Je crée mon compte Plantera <a href="#" id="signup">Signup</a>
                </div>
            </div>
        </form>

    </div>

    <script src="app.js"></script>
</body>

</html>