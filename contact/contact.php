<?php include "../header/header.php"; ?>
<link rel="stylesheet" href="styles.css">


<h2 class="king">Notre Equipe </h2>

<div class="container1">
    <div class="box">
        <div class="logoBox"><img class="logo1"
                src="../stock/ress/entrepreneur.png"></img>
        </div>
        <div class="boxcentre">
            <div class="test">
                <div class="NOM">
                    <p>@AurelienJeune</p>
                    <div>
                        <div class="metier">
                            <p>Fleuriste</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text">
        <p class="koho"> En tant que discipline scientifique, l’écologie s’appuie sur les progrès continus de la
            connaissance au cours de la fin du XIXe siècle puis du XXe, et se documente au carrefour de toutes les
            disciplines liées de près ou de loin à la biologie, telles que la génétique, l’éthologie ou encore la
            géologie et la climatologie. Son objectif est principalement de comprendre la complexité des écosystèmes
            naturels. La discipline va continuer à se développer jusqu’à aujourd’hui.
            Mais l’écologie devient aussi une idée politique : les prémices de l’écologie politique apparaissent
            également en Occident, dès la deuxième moitié du XIXe siècle, en réaction à l’avènement de l’ère
            industrielle. Face aux pollutions et aux dégradations de la nature engendrées par les activités économiques
            et industrielles, de plus en plus d’individus vont commencer à revendiquer une forme de protection
            nécessaire de la nature. Cette idée va se développer tout au long du 20ème siècle mais c’est surtout à
            partir des années 1960 que ce courant de pensée, va devenir un vrai mouvement politique.
            En France, c’est à peu près à cette époque que des voix commencent à s’élever pour faire de l’écologie un
            projet politique. En 1961 est fondée l’ONG WWF, puis en 1970 ce sont les Amis de la Terre. Dans le même
            temps, dès 1974, l’élection présidentielle française est marquée par la première participation d’un candidat
            ouvertement écologiste : René Dumont. Dans le monde, c’est aussi à cette période que l’idée de l’écologie
            politique se développe. Aux Etats-Unis, la publication du livre Silent Sprint par Rachel Carson en 1962
            alerte sur les dangers de la crise écologique. S’en suit en 10 ans plus tard la publication du Rapport
            Meadows (Halte à la Croissance) par des chercheurs du MIT mettant en évidence les dangers écologiques de la
            croissance économique mondiale. Depuis, la prise de conscience écologique n’a cessé de se développer.</p>
    </div>
</div>


<div class="container2">
    <div class="box2">
        <div class="logoBox2"><img class="logo2"
                src="../stock/ress/entrepreneur2.png"></img>
        </div>
        <div class="boxcentre2">
            <div class="test2">
                <div class="NOM2">
                    <p>@ValerieMarie </p>
                    <div>
                        <div class="metier2">
                            <p>SEO</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text2">
        <p class="koho">
            Je pense que ce qui vous caractérise le mieux au-delà d'une <b>très grande diversité</b> qui est
            évidente, c'est la volonté de prendre des risques et d'assumer qu'il est indispensable de savoir en prendre.
            Un goût de l'innovation, je dirais même un certain sens de la débrouillardise car lorsqu'on crée, lorsqu'on
            se lance, lorsqu’on innove, on est tenu ou plus exactement contraint de devoir s'adapter à des choses
            auxquelles au fond on n'est pas préparé. Ça a toujours été comme ça, je pense que ce sera toujours comme ça.
            On peut essayer de faciliter les choses, de limiter la dose d'agacement à subir, elle sera probablement
            toujours là, et ce qui vous caractérise c'est justement de ne pas vous arrêter à cela et d'arriver à trouver
            des solutions concrètes pour continuer à avancer.</p>
    </div>
</div>
<div id="sectionContact">
<h2 class="king">Contactez nous</h2>
<div class="wrapper">
<form class="form" action="../controller/contactAcc.php" method="post" onsubmit="return checkForm(this);">
        <cite>* Champs obligatoires</cite>
        <div class="contact">
        <div class="form-group">
                <input name="nom" type="text" placeholder="Votre Nom " maxlength="25" autocomplete="off"
            required />

            </div>
            <div class="form-group">
                <input name="email"type="mail" placeholder="Votre addresse email " pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" autocomplete="off" required />
            </div>
            <div class="form-group">
                <textarea name="comment" name="description" placeholder="Votre Message " cols="80" rows="20"
                    class="textarea-product"autocomplete="off"
            required></textarea>

            </div>

            <div class="elem-group">
            <div class="rowCap">
                <label for="captcha">Veuillez entrer le captcha</label>
                <img src="../controller/captcha.php" alt="CAPTCHA" class="captcha-image"><i
                    class="fas fa-redo refresh-captcha" aria-hidden="true"></i>
            </div>
            <br>
            <div class="CapValide">
                <p><input class="captchaInput" id="captcha_code_input" type="text" name="captcha" size="6" maxlength="5"
                        onkeyup="this.value = this.value.replace(/[^\d]+/g, '');"> </p>
            </div>
            <div id="msg">
                <?php 
            if(isset($_GET['msg'])){
                echo"(".$_GET['msg'].")";
                
            }?>
            </div>
        </div>



            <button type="submit" name="submit"value="Show Div" onclick="showDiv()" class="btn">Envoyer </button>
        </div>
    </form>
</div>
        </div>
        <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
<script src="contact.js"></script>
<?php include "../footer/footer.php"; ?>
