var refreshButton = document.querySelector("i.refresh-captcha");
console.log(refreshButton)
refreshButton.onclick = function () {
  document.querySelector(".captcha-image").src =
    "../controller/captcha.php?" + Date.now();
};

function checkForm(form) {
  if (!form.captcha.value.match(/^\d{5}$/)) {
    alert("Please enter the CAPTCHA digits in the box provided");
    form.captcha.focus();
    return false;
  }

  return true;
}


// function showDiv() {
//   document.querySelector('.elem-group').style.display = "block";
// }


