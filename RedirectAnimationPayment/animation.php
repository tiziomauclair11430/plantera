<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet/less" type="text/css" href="animation.less" />
    <title>Document</title>
</head>

<body onLoad="setTimeout('RedirectionJavascript()', 5000)">

    <div class="payment-loader">
        <div class="pad">
            <div class="chip"></div>
            <div class="line line1"></div>
            <div class="line line2"></div>
        </div>
        <div class="loader-text">
            <P>Redirection vers la page de payment...</P>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/less@4"></script>
    <script>
        function RedirectionJavascript(){
        document.location.href="../stripe-payment-gateway-integration-php/pay.php"; 
        }
    </script>
</body>