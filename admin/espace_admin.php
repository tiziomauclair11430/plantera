<?php
session_start();
include '../BDD/data.php';
include '../protected/protected.php';
// include "../debug/debug.php";

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Espace Admin</title>
    <link rel="stylesheet" href="espace_admin.css">
</head>

<body>

    <div class="admin-panel clearfix">
        <div class="slidebar">
            <div class="logo">
                <a href=""></a>
            </div>
            <ul>
                <li><a href="#dashboard" id="targeted">Dashboard</a></li>
                <li><a href="#ajouter-produit">Ajouter un produit</a></li>
                <li><a href="#modifier-produit">Modifier un produit</a></li>
                <li><a href="#voir-produit">Voir Produit</a></li>
                <li><a href="#voir-commande">Voir commande</a></li>
                <li><a href="#Messages">Messages</a></li>
                <li><a href="#actualites">Ajouter des Actualites</a></li>
                <li><a href="#voir-actualites">voir les Actualites</a></li>
                <li><a href="#modifier-actualites">Modifier les Actualites</a></li>
                <li><a href="#Parametre-General">Parametre General</a></li>
                <li><a href="../home/index.php">Retour boutique</a></li>
                <li><a href="../sessiondelete/sessiondelete.php">Deconnexion</a></li>
            </ul>
        </div>

        <div class="main">
            <div class="mainContent clearfix">
                <div id="dashboard">
                    <h2 class="header"><span class="icon"></span>Dashboard</h2>
                    <div class="welcome-card">
                        <h1>Bienvenue ValerieMarie!</h1>
                        <p>
                            ✌️Hey, je vous souhaite la bienvenue sur votre espace admin.
                        </p>
                    </div>
                </div>
                <div id="ajouter-produit">
                    <h2 class="header">Ajouter produit</h2>
                    <form class="form-size" action="../controller/insert.php" method="post" enctype="multipart/form-data">
                        <div class="div-input-product">
                            <label for="nom" class="label-product">Nom du produit</label>
                            <input type="text" name="nom" class="input-product">
                        </div>
                        <div class="div-carousel-product">
                            <div class="option" id="carousel">
                                <input type="hidden" value="1" id="nbrImg" name="nbrImg">
                                <button onclick="nvxImg()" type="button">Add new photos</button>
                                <div class="carrousel-control" id="carrousel-control">
                                    <input type="number" id="placement">
                                    <button type="button">...12345</button>
                                    <button type="button">54321...</button>
                                    <input type="file" name="img1" accept="image/*" class="input-carousel" id="img1" onchange="previewPicture(this)">
                                </div>
                            </div>
                            <span id="compteur" class="compteur"></span>
                            <div class="carrousel" id="control-fleche">
                                <div id="gauche"></div>
                                <img id="image" class="rendu-image">
                                <div id="droite"></div>
                            </div>
                        </div>
                        <div class="div-textarea-product">
                            <label for="description" class="label-product">Description</label>
                            <textarea name="description" cols="80" rows="20" class="textarea-product"></textarea>
                        </div>

                        <div class="div-textarea-product">
                            <label for="Minidescription" class="label-product">Mini Description</label>
                            <textarea name="Minidescription" cols="80" rows="5" placeholder="125 caractères maximum!" class="textarea-product"></textarea>
                        </div>

                        <div class="div-input-product">
                            <label for="temperature" class="label-product">Temperature</label>
                            <input type="text" name="temperature" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="humidite" class="label-product">Humidite</label>
                            <input type="text" name="humidite" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="temps" class="label-product">Temps</label>
                            <input type="text" name="temps" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="stock" class="label-product">Stock</label>
                            <input type="number" name="stock" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="prix" class="label-product">Prix</label>
                            <input type="number" step=any name="prix" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="si_solde" class="label-product">si solde</label>
                            <input type="checkbox" name="si_solde" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="Prixsolde" class="label-product">Solde</label>
                            <input type="number" name="Prixsolde" id="" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="livraison" class="label-product">Livraison</label>
                            <input type="checkbox" name="livraison" id="" class="input-product">
                        </div>

                        <input type="submit" value="Ajouter">
                    </form>


                </div>
                <div id="modifier-produit">
                    <h2 class="header">Modifier un produit</h2>
                    <?php
                    $id = $_POST['modif'];
                    if (isset($_POST['modif'])) {
                        $functionGetProduit = getProduitById($id);
                    } else {
                        print "<p class='error'>Veuillez selectionner un produit dans la categorie 'voir produit' et cliquer sur le bouton 'Modifier'</p>";
                    }
                    foreach ($functionGetProduit as $select) {
                    ?>
                        <form class="" action="../controller/update.php" method="post" enctype="multipart/form-data">
                            <p>nom :</p>
                            <input type="text" id="nom" name="nom" class="input-product" value="<?php echo $select['nom']; ?>">

                            <p>img :</p>
                            <input type="image" src="../stock/uploads/<?php echo $select['url']?>" alt="">
                            <input type="file" id="img" name="img" type="file" accept="image/*" class="input-carousel" value="Choissir un fichier si photo n'est pas bonne">

                            <p>description :</p>
                            <textarea name="description" id="description" cols="80" rows="20" class="textarea-product" value=""><?php echo $select['description']; ?></textarea>

                            <p>Minidescription :</p>
                            <textarea name="Minidescription" id="Minidescription" cols="80" rows="10" class="textarea-product" value=""><?php echo $select['descriptionMini']; ?></textarea>

                            <p>temperature :</p>
                            <input type="text" name="temperature" id="temperature" class="input-product" value="<?php echo $select['temperature']; ?>">

                            <p>humidite :</p>
                            <input type="text" name="humidite" id="humidite" class="input-product" value="<?php echo $select['humidite']; ?>">

                            <p>temps :</p>
                            <input type="text" name="temps" id="temps" class="input-product" value="<?php echo $select['temps']; ?>">

                            <p>stock :</p>
                            <input type="number" name="stock" id="stock" class="input-product" value="<?php echo $select['stock']; ?>">

                            <p>prix :</p>
                            <input type="number" name="prix" id="prix" class="input-product" value="<?php echo $select['prix']; ?>">

                            <p>si_solde :</p>
                            <input type="checkbox" name="si_solde" id="si_solde" class="input-product" value="<?php echo $select['si_solde']; ?>">

                            <p>Prixsolde :</p>
                            <input type="number" name="Prixsolde" id="Prixsolde" class="input-product" value="<?php echo $select['prix_solde']; ?>">

                            <p>livraison :</p>
                            <input type="checkbox" name="livraison" id="livraison" class="input-product" value="<?php echo $select['si_livraison']; ?>">
                            <input type="hidden" name="id" value="<?php echo $select['id']; ?>">
                            <button class="" type="submit">Modifier Produit</button>
                        </form>
                    <?php } ?>
                </div>
                <div id="voir-produit">
                    <h2 class="header">Voir Produit</h2>
                    <div class="containerTable">
                        <div class="form-control">
                            <label for="search"><i class="icon-search"></i></label>
                            <input class="table-filter" type="search" data-table="advanced-web-table" placeholder="Search...">
                            <button onclick="exportData()">
                                <i class="fas fa-download"></i>
                                Download
                            </button>
                        </div>
                        <!--  Table  -->
                        <div class="table-responsive">
                            <table id="ordering-table" class="advanced-web-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>img</th>
                                        <th>nom</th>
                                        <th>description</th>
                                        <th>descriptionMini</th>
                                        <th>temperature</th>
                                        <th>humidite</th>
                                        <th>temps</th>
                                        <th>stock</th>
                                        <th>prix</th>
                                        <th>si_solde</th>
                                        <th>prix_solde</th>
                                        <th>si_livraison</th>
                                        <th>date</th>
                                        <th>modif</th>
                                        <th>sup</th>
                                    </tr>
                                </thead>
                                <?php

                                $list_pro = getProduit();

                                ?>
                                <tbody>
                                    <tr>
                                        <?php
                                        foreach ($list_pro as $select) {
                                        ?>
                                            <td><?php echo $select['id']; ?></td>
                                            <td>img</td>
                                            <td><?php echo $select['nom']; ?></td>
                                            <td style="min-width: 400px;"><?php echo $select['description']; ?></td>
                                            <td><?php echo $select['descriptionMini']; ?></td>
                                            <td><?php echo $select['temperature']; ?></td>
                                            <td><?php echo $select['humidite']; ?></td>
                                            <td><?php echo $select['temps']; ?></td>
                                            <td><?php echo $select['stock']; ?></td>
                                            <td><?php echo $select['prix']; ?></td>
                                            <td><?php echo $select['si_solde']; ?></td>
                                            <td><?php echo $select['prix_solde']; ?></td>
                                            <td><?php echo $select['si_livraison']; ?></td>
                                            <td><?php echo $select['dateproduct']; ?></td>
                                            <td>
                                                <form action="../admin/espace_admin.php#modifier-produit" method="post">
                                                    <button type="submit" name="modif" value="<?php echo $select['id'] ?>"><i class="fas fa-pen"></i></button></button>
                                                </form>
                                            </td>
                                            <td>
                                                <form action="../controller/delete.php" method="post">

                                                    <button type="submit" name="supp" value="<?php echo $select['id'] ?>"><i class="fas fa-trash"></i></button>
                                                </form>
                                            </td>



                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="voir-commande">
                    <h2 class="header">Voir commande</h2>
                </div>
                <div id="Messages">
                    <h2 class="header">Messages</h2>
                    <?php

                    $list_mail = getAllMail();

                    ?>


                    <table class="GeneratedTable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>nom</th>
                                <th>email</th>
                                <th>comment</th>
                                <th>Date</th>
                                <th>Supprimer</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($list_mail as $f) {
                            ?>
                                <tr>
                                    <td><?php echo $f["id"] ?></td>
                                    <td><?php echo $f["nom"] ?></td>
                                    <td><a href="mailto: abc@example.com"><?php echo $f["email"] ?></a></td>
                                    <td><?php echo $f["comment"] ?></td>
                                    <td><?php echo $f["datemail"]  ?></td>
                                    <td>
                                        <div class="delt">
                                            <form class="test" action="../controller/deleteMail.php" method="post">

                                                <button type="submit" name="supp" value="<?php echo $f['id'] ?>"><i class="fas fa-trash"></i></button>
                                            </form>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>


                <div id="actualites">
                    <h2 class="header">Actualites</h2>
                    <form class="form-size" action="../controller/insertActualites.php" method="post" enctype="multipart/form-data">

                        <div class="div-input-product">
                            <label for="photo" class="label-product">photo </label>
                            <input class="fichier" name="photo" type="file" accept="image/*">
                        </div>

                        <div class="div-input-product">
                            <label for="title" class="label-product">title</label>
                            <input type="text" name="title" class="input-product">
                        </div>

                        <div class="div-input-product">
                            <label for="sous_title" class="label-product">sous_title</label>
                            <input type="text" name="sous_title" class="input-product">
                        </div>
                        <div class="div-textarea-product">
                            <label for="card_description" class="label-product">card description</label>
                            <textarea name="card_description" cols="80" rows="5" class="textarea-product"></textarea>
                        </div>
                        <button id="toggle" onclick="pop_up()" type="button">Voir</button>
                        <span id="span_actu" style="display: none;">
                            <div class="projcard-container">
                                <div class="projcard projcard-customcolor" style="--projcard-color: #F5AF41;">
                                    <div class="projcard-innerbox">
                                        <img class="projcard-img" src="../stock/uploads/exemple1.jpeg" />
                                        <div class="projcard-textbox">
                                            <div class="projcard-title"></div>
                                            <div class="projcard-subtitle"></div>
                                            <div class="projcard-bar"></div>
                                            <div class="projcard-description"></div>
                                            <div class="projcard-tagbox">
                                                <span class="projcard-tag"></span>
                                                <span class="projcard-tag"></span>
                                                <span class="projcard-tag"></span>

                                            </div>
                                            <div class="boxDA">
                                                <span class="date"><?php echo date('y-m-d') ?><span>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </span>
                        <div class="div-input-product">
                            <label for="mots_cles1" class="label-product">mots_cles1</label>
                            <input type="text" name="mots_cles1" class="input-product">
                        </div>
                        <div class="div-input-product">
                            <label for="mots_cles2" class="label-product">mots_cles2</label>
                            <input type="text" name="mots_cles2" class="input-product">
                        </div>
                        <div class="div-input-product">
                            <label for="mots_cles3" class="label-product">mots_cles3</label>
                            <input type="text" name="mots_cles3" class="input-product">
                        </div>
                        <div class="div-textarea-product">
                            <label for="paragraph1" class="label-product">paragraph1</label>
                            <textarea name="paragraph1" cols="80" rows="20" class="textarea-product"></textarea>
                        </div>
                        <div class="div-textarea-product">
                            <label for="paragraph2" class="label-product">paragraph2</label>
                            <textarea name="paragraph2" cols="80" rows="20" class="textarea-product"></textarea>
                        </div>
                        <div class="div-textarea-product">
                            <label for="paragraph3" class="label-product">paragraph3</label>
                            <textarea name="paragraph3" cols="80" rows="20" class="textarea-product"></textarea>
                        </div>



                        <div class="div-input-product">
                            <label for="author" class="label-product">author</label>
                            <input type="text" name="author" class="input-product">
                        </div>

                        <input type="submit" value="Ajouter">
                    </form>
                </div>

                <div id="Parametre-General">
                    <h2 class="header">Parametre General</h2>
                </div>


                <div id="voir-actualites">
                    <h2 class="header">Voir-actualites</h2>

                    <div class="containerTable">
                        <div class="form-control">
                            <label for="search"><i class="icon-search"></i></label>
                            <input class="table-filter" type="search" data-table="advanced-web-table" placeholder="Search...">
                        </div>
                        <!--  Table  -->
                        <div class="table-responsive">
                            <table id="ordering-table" class="advanced-web-table">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>photo</th>
                                        <th>title</th>
                                        <th>sous_title</th>
                                        <th>card_description</th>
                                        <th>mots_cles1</th>
                                        <th>mots_cles2</th>
                                        <th>mots_cles3</th>
                                        <th>paragraph1</th>
                                        <th>paragraph2</th>
                                        <th>paragraph3</th>
                                        <th>author</th>
                                        <th>Date</th>
                                        <th>modif</th>
                                        <th>sup</th>
                                    </tr>
                                </thead>
                                <?php

                                $list_act = getActualites();

                                ?>
                                <tbody>
                                    <tr>
                                        <?php
                                        foreach ($list_act as $select) {
                                        ?>
                                            <td><?php echo $select['id']; ?></td>
                                            <td><img class="modif-img" src="../stock/uploads/<?php echo $select['photo'] ?>" /></td>
                                            <td><?php echo $select['title']; ?></td>
                                            <td><?php echo $select['sous_title']; ?></td>
                                            <td style="min-width: 400px;"><?php echo $select['card_description']; ?></td>
                                            <td><?php echo $select['mots_cles1']; ?></td>
                                            <td><?php echo $select['mots_cles2']; ?></td>
                                            <td><?php echo $select['mots_cles3']; ?></td>
                                            <td style="min-width: 400px;"><?php echo $select['paragraph1']; ?></td>
                                            <td style="min-width: 400px;"><?php echo $select['paragraph2']; ?></td>
                                            <td style="min-width: 400px;"><?php echo $select['paragraph3']; ?></td>
                                            <td><?php echo $select['author']; ?></td>
                                            <td><?php echo $select['Ladate']; ?></td>
                                            <td>
                                                <form action="../admin/espace_admin.php#modifier-actualites" method="post">
                                                    <button type="submit" name="modif" value="<?php echo $select['id'] ?>"><i class="fas fa-pen"></i></button></button>
                                                </form>
                                            </td>
                                            <td>
                                                <form action="../controller/deleteActualites.php" method="post">

                                                    <button type="submit" name="supp" value="<?php echo $select['id'] ?>"><i class="fas fa-trash"></i></button>
                                                </form>
                                            </td>



                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


                <div id="modifier-actualites">
                    <h2 class="header">Modifier actualites</h2>
                    <?php
                    $id = $_POST['modif'];
                    if (isset($_POST['modif'])) {
                        $functionGetProduit = getctualitesById($id);
                    } else {
                        print "<p class='error'>Veuillez selectionner un element dans la categorie 'voir Actualites' et cliquer sur le bouton 'Modifier'</p>";
                    }
                    foreach ($functionGetProduit as $select) {
                    ?>
                        <form class="" action="../controller/updateActualites.php" method="post" enctype="multipart/form-data">

                            <p>photo :</p>
                            <input type="file" id="photo" name="photo" type="file" accept="image/*" class="input-carousel">

                            <input type="hidden" name="photo" value="<?php echo $select['photo']; ?>">

                            <p>title :</p>
                            <input type="text" id="title" name="title" class="input-product" value="<?php echo $select['title']; ?>">

                            <p>sous_title :</p>
                            <input type="text" name="sous_title" id="sous_title" class="input-product" value="<?php echo $select['sous_title']; ?>">

                            <p>card_description :</p>
                            <textarea name="card_description" id="card_description" cols="80" rows="20" class="textarea-product" value=""><?php echo $select['card_description']; ?></textarea>


                            <p>mots_cles1:</p>
                            <input type="text" name="mots_cles1" id="mots_cles1" class="input-product" value="<?php echo $select['mots_cles1']; ?>">

                            <p>mots_cles2:</p>
                            <input type="text" name="mots_cles2" id="mots_cles2" class="input-product" value="<?php echo $select['mots_cles2']; ?>">

                            <p>mots_cles3:</p>
                            <input type="text" name="mots_cles3" id="mots_cles3" class="input-product" value="<?php echo $select['mots_cles3']; ?>">


                            <p>paragraph1 :</p>
                            <textarea name="paragraph1" id="paragraph1" cols="80" rows="10" class="textarea-product" value=""><?php echo $select['paragraph1']; ?></textarea>

                            <p>paragraph2 :</p>
                            <textarea name="paragraph2" id="paragraph2" cols="80" rows="10" class="textarea-product" value=""><?php echo $select['paragraph2']; ?></textarea>

                            <p>paragraph3 :</p>
                            <textarea name="paragraph3" id="paragparagraph3raph1" cols="80" rows="10" class="textarea-product" value=""><?php echo $select['paragraph3']; ?></textarea>

                            <p>author :</p>
                            <input type="text" name="author" id="author" class="input-product" value="<?php echo $select['author']; ?>">



                            <button class="" type="submit" name="id" value="<?php echo $select['id']; ?>">Modifier
                                actualite</button>
                        </form>
                    <?php } ?>

                </div>
                <!-- <div id="Deconnexion">
                    <h2 class="header">Deconnexion</h2>
                </div> -->
            </div>
        </div>
    </div>
    <script src="https://kit.fontawesome.com/520b85ccf6.js" crossorigin="anonymous"></script>
    <script src="script.js"></script>
    <script src="filter.js"></script>
    <script src="exportData.js"></script>
    <script>
    </script>
</body>

</html>